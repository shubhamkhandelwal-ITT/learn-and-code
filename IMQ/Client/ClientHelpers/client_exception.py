

class ClientException(Exception):

    def what(self):
        pass

class ArgumentInvalidException(ClientException):
    error_message = 'Invalid argument'
    
    def what(self):
        print(self.error_message)

class ParsingFailedException(ClientException):
    error_message = 'Parsing Failed'

    def what(self):
        print(self.error_message)

class SocketErrorException(ClientException):
    error_message = 'Socket Error Exception'

    def what(self):
        print(self.error_message)

class ConnectionNotCreatedException(ClientException):
    error_message = 'Connection not created Exception'

    def what(self):
        print(self.error_message)