import socket 
import json
from ClientHelpers.client_exception import SocketErrorException, ConnectionNotCreatedException, ClientException
from ClientHelpers.constants import ENCODE_TYPE, CONNECT_ESTABLISH, COLOR_GREEN
from termcolor import colored

class Client:
    _client_socket = None

    def __init__(self):
        try:
            self._client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        except:
            raise SocketErrorException()

    def connect_to_server(self, host, port):
        try:
            self._client_socket.connect((host,port))
            print(colored(CONNECT_ESTABLISH, COLOR_GREEN))
        except:
            raise SocketErrorException()
            

    def send_message_to_server(self, message):

        try:
            if self._client_socket != None:
                json_obj = json.dumps(message.__dict__)
                self._client_socket.send(json_obj.encode(ENCODE_TYPE))
            else:
                raise ConnectionNotCreatedException()
        except:
            raise ClientException
	
    def get_client_socket(self):
        return self._client_socket
    
    def __del__(self):
        self._client_socket.close()
