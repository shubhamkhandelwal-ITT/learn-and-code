from datetime import datetime
from IMQ_common.imq_protocol import ImqRequest, ImqResponse
from IMQ_common.constants import ImqDataFormat, ResponseType, RequestType

class ClientUtility:
    _source_url: None
    _destination_url: None
    _version: None
    _data_format: None
    _client_id: None
    _client_name: None

    def __init__(self, source_url, destination_url , version, data_format: ImqDataFormat, client_id, client_name):
        self._source_url = source_url
        self._destination_url = destination_url
        self._version = version
        self._data_format = data_format.value
        self._client_id = client_id
        self._client_name = client_name

    def create_request(self, request_type: RequestType, data: str):
        request_object = ImqRequest()
        request_object.data = data
        request_object.data_format = self._data_format
        request_object.source_url = self._source_url
        request_object.destination_url = self._destination_url
        request_object.set_unique_id()
        request_object.request_type = request_type.value
        request_object.version = self._version
        request_object.client_id = self._client_id
        request_object.client_name = self._client_name
        request_object.timestamp = str(datetime.now())

        return request_object
