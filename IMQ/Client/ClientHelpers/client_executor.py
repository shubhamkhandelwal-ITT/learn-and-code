import threading
import sys
from ClientHelpers.client import Client
from IMQ_common.constants import  RequestType
from ClientHelpers.client_utility import ClientUtility
from ClientHelpers.client_exception import ArgumentInvalidException
from CommandLine.client_supported_command import SupportedCommands
from ClientHelpers.constants import *
class ClientExecutor:
    execute_command: None
    client_utility_obj: None
    user_client: None
    __supported_commands: None

    def __init__(self, client_utility_obj : ClientUtility, user_client : Client):
        self.__load_executor_of_commands()
        self.client_utility_obj = client_utility_obj
        self.user_client = user_client
        self.__supported_commands = SupportedCommands.commands

    def __connect(self, args: dict) -> bool:

        if self.__validate_arguments(CONNECT_COMMAND_KEYWORD, args.keys()):
            username = args[CONNECT_COMMAND_ARGUMENT]
            data = {}
            data[CONNECT_COMMAND_DATA_KEY] = username
            request_message = self.client_utility_obj.create_request(
                RequestType.SETUP_CONNECTION, data)
            return self.user_client.send_message_to_server(request_message)
        
        else:
            raise ArgumentInvalidException()

    def __disconnect(self, args) -> bool:

        if self.__validate_arguments(DISCONNECT_COMMAND_KEYWORD, args.keys()):
            data = {MESSAGE_DATA_KEY: CLOSING_RECIVING_THREAD}
            request_message = self.client_utility_obj.create_request(
                RequestType.CLOSE, data)

            self.user_client.send_message_to_server(request_message)
            cliet_socket = self.user_client.get_client_socket()
            cliet_socket.close()
            return True
        else:
            raise ArgumentInvalidException()
        
        
    def __show(self, args) -> bool:

        if self.__validate_arguments(SHOW_COMMAND_KEYWORD, args.keys()):
            if TOPIC_KEY in args.keys():
                request_message = self.client_utility_obj.create_request(
                    RequestType.TOPIC_LIST, {})
                return self.user_client.send_message_to_server(request_message)
            
            elif MESSAGE_KEY in args.keys():
                data = {}
                data[TOPIC_NAME_DATA_KEY] = args[MESSAGE_KEY]
                request_message = self.client_utility_obj.create_request(
                    RequestType.PULL_TOPIC_DATA, data)
                return self.user_client.send_message_to_server(request_message)
        else:
            raise ArgumentInvalidException()
    
    def __status(self, args)-> bool:

        if self.__validate_arguments(STATUS_COMMAND_KEYWORD, args.keys()):
            data = {}
            data[TOPIC_NAME_DATA_KEY] = args[TOPIC_KEY]
            request_message = self.client_utility_obj.create_request(
                RequestType.STATUS_OF_TOPIC, data)
            return self.user_client.send_message_to_server(request_message)
        else:
            raise ArgumentInvalidException()

    def __add(self, args)-> bool:
        if self.__validate_arguments(ADD_COMMAND_KEYWORD, args.keys()):
            data = {}
            data[TOPIC_NAME_DATA_KEY] = args[TOPIC_KEY]
            request_message = self.client_utility_obj.create_request(
                RequestType.ADD_TOPIC, data)
            return self.user_client.send_message_to_server(request_message)
        else:
            raise ArgumentInvalidException()

    def __publish(self, args)-> bool:
        if self.__validate_arguments(PUBLISH_COMMAND_KEYWORD, args.keys()):
            data = {}
            data[TOPIC_NAME_DATA_KEY] = args[TOPIC_KEY]
            data[PUBLISH_DATA_KEY] = args[MESSAGE_KEY]
    
            request_message = self.client_utility_obj.create_request(
                RequestType.PUBLISH_TOPIC_DATA, data)
            return self.user_client.send_message_to_server(request_message)
        else:
            raise ArgumentInvalidException()

    def __load_executor_of_commands(self):
        self.execute_command = {}
        self.execute_command[CONNECT_COMMAND_KEYWORD] = self.__connect
        self.execute_command[DISCONNECT_COMMAND_KEYWORD] = self.__disconnect
        self.execute_command[SHOW_COMMAND_KEYWORD] = self.__show
        self.execute_command[ADD_COMMAND_KEYWORD] = self.__add
        self.execute_command[PUBLISH_COMMAND_KEYWORD] = self.__publish
        self.execute_command[STATUS_COMMAND_KEYWORD] = self.__status

    
    def __validate_arguments(self, support_command, args: list) -> bool:
        supported_arguments = self.__supported_commands[support_command]
        argument_valid = False
        for arguments in supported_arguments:
            if list(args) == arguments:
                argument_valid = True
                break
        
        return argument_valid


