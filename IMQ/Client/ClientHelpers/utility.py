import uuid
import socket
import math
import threading
import json
from IMQ_common.constants import ResponseType
from ClientHelpers.constants import *
from termcolor import colored

def create_client_id(username:str):
    unique_id = int(uuid.uuid5(uuid.NAMESPACE_DNS, username))
    client_id = round(math.log(unique_id) * 100)

    return client_id

def recieve_from_server(client_socket: socket.SocketIO):

    try:
        while True:

            # data received from client
            data = client_socket.recv(RECIVING_BUFFER_SIZE)
            data = data.decode(ENCODE_TYPE)
            data = json.loads(data)
            message = (data[TIMESTAMP_KEY], data[RESPONSE_TYPE_KEY], data[DATA_KEY])
            if data[RESPONSE_TYPE_KEY] == ResponseType.SUCCESS.value:
                print(colored(message, COLOR_GREEN))
            else:
                print(colored(message, COLOR_RED))
    except:
        print(CLOSING_RECIVING_THREAD)
