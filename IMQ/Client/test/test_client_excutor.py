import unittest
import sys
import os
import socket
sys.path.insert(0,'../')
sys.path.append(os.getcwd())
# print(os.getcwd())
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from ClientHelpers.client_executor import ClientExecutor, Client, ClientUtility
from unittest.mock import Mock, create_autospec, patch
from ClientHelpers.client_exception import ArgumentInvalidException

class TestClientExecutor(unittest.TestCase):

    mock_socket = create_autospec(socket.socket)
    mock_socket.close.return_value = True

    mock = create_autospec(Client)
    mock.send_message_to_server.return_value = True
    mock.get_client_socket.return_value = mock_socket

    client_id = 123
    username = 'shubham'
    client_utility_obj = ClientUtility(socket.gethostname(), Constants.HOST, 'v1', ImqDataFormat.JSON, client_id, username)
    __client_executor: None

    def test_client_executor_connect(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {'-u': 'username'}
        result = self.__client_executor.execute_command['connect'](args)
        assert True == result
    
    def test_client_executor_connect_exception(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {}
        self.assertRaises(ArgumentInvalidException ,self.__client_executor.execute_command['connect'], args)
    
    def test_client_executor_disconnect(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {}
        result = self.__client_executor.execute_command['disconnect'](args)
        assert True == result
    
    def test_client_executor_disconnect_exception(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {'-u': 'username'}
        self.assertRaises(ArgumentInvalidException ,self.__client_executor.execute_command['disconnect'], args)
    
    def test_client_executor_publish(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {'-t': 'topic', '-m': 'message'}
        result = self.__client_executor.execute_command['publish'](args)
        assert True == result
    
    def test_client_executor_publish_exception(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {}
        self.assertRaises(ArgumentInvalidException ,self.__client_executor.execute_command['publish'], args)
    
    def test_client_executor_add_topic(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {'-t': 'topic'}
        result = self.__client_executor.execute_command['add'](args)
        assert True == result
    
    def test_client_executor_add_topic_exception(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {}
        self.assertRaises(ArgumentInvalidException ,self.__client_executor.execute_command['add'], args)
    
    def test_client_executor_show_topic_list(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {'-t': 'topic'}
        result = self.__client_executor.execute_command['show'](args)
        assert True == result
    
    def test_client_executor_show_topic_list_exception(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {}
        self.assertRaises(ArgumentInvalidException ,self.__client_executor.execute_command['show'], args)
    
    def test_client_executor_show_message(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {'-m': 'topicname'}
        result = self.__client_executor.execute_command['show'](args)
        assert True == result
    
    def test_client_executorshow_message_exception(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {}
        self.assertRaises(ArgumentInvalidException ,self.__client_executor.execute_command['show'], args)
    
    def test_client_executor_status(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {'-t': 'topicname'}
        result = self.__client_executor.execute_command['status'](args)
        assert True == result
    
    def test_client_executor_status_exception(self):
        self.__client_executor = ClientExecutor(self.client_utility_obj, self.mock)
        args = {}
        self.assertRaises(ArgumentInvalidException ,self.__client_executor.execute_command['status'], args)
