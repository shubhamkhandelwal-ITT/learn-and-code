import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())
from ClientHelpers.utility import create_client_id, recieve_from_server
from unittest.mock import Mock, create_autospec, patch

class TestUtility(unittest.TestCase):


    def test_create_client_id(self):
        assert 8792 == create_client_id('shubham')
    
    def test_message_recieve_from_server(self):
        data = None
        mock_socket = create_autospec(socket.socket)
        mock_socket.recv.return_value = data
        recieve_from_server(mock_socket)
        assert mock_socket.recv.called