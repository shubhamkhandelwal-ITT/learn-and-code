import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())

from ClientHelpers.client_utility import ClientUtility
from IMQ_common.constants import ImqDataFormat, RequestType, Constants

class TestClientUtility(unittest.TestCase):
    __client_utility: None
    def test_request_object_example(self):
        client_id = 123
        username = 'shubham'
        data = {}
        self.__client_utility = ClientUtility(socket.gethostname(), Constants.HOST, 'v1', ImqDataFormat.JSON, client_id, username)
        request_object = self.__client_utility.create_request(RequestType.PUBLISH_TOPIC_DATA, data)

        assert client_id == request_object.client_id
        assert username == request_object.client_name
        assert data == request_object.data
    
    def test_request_object_example_2(self):
        client_id = 124
        username = 'devraj'
        data = {}
        self.__client_utility = ClientUtility(socket.gethostname(), Constants.HOST, 'v1', ImqDataFormat.JSON, client_id, username)
        request_object = self.__client_utility.create_request(RequestType.PULL_TOPIC_DATA, data)

        assert client_id == request_object.client_id
        assert username == request_object.client_name
        assert data == request_object.data
        assert RequestType.PULL_TOPIC_DATA.value == request_object.request_type

