import unittest
import sys
import os
sys.path.append(os.getcwd())
from ClientHelpers.client_exception import ParsingFailedException
from CommandLine.imq_cli import IMQ_CLI

class TestIMQCli(unittest.TestCase):
    __imq_cli: None
    def test_parse_for_connect_command(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("imq connect -u username")

        assert True == parse
        assert "connect" == support_command
        assert {'-u': 'username'} == args
    
    def test_parse_for_disconnect_command(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("imq disconnect")

        assert True == parse
        assert "disconnect" == support_command
        assert {} == args
    
    def test_parse_for_show_message_command(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("imq show -t topic")

        assert True == parse
        assert "show" == support_command
        assert {'-t': 'topic'} == args
    
    def test_parse_for_show_topic_command(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("imq show -m topic")

        assert True == parse
        assert "show" == support_command
        assert {'-m': 'topic'} == args
    
    def test_parse_for_status_command(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("imq status -t topic")

        assert True == parse
        assert "status" == support_command
        assert {'-t': 'topic'} == args
    
    def test_parse_for_add_topic_command(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("imq add -t topic")

        assert True == parse
        assert "add" == support_command
        assert {'-t': 'topic'} == args
    
    def test_parse_for_publish_command(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("imq publish -t topic -m everything")

        assert True == parse
        assert "publish" == support_command
        assert {'-t': 'topic', '-m': 'everything'} == args

    def test_parse_for_wrong_command_1(self):
        self.__imq_cli = IMQ_CLI()
        self.assertRaises(ParsingFailedException ,self.__imq_cli.parse_command, 'imq publish')

    def test_parse_for_wrong_command_2(self):
        self.__imq_cli = IMQ_CLI()
        parse, support_command, args = self.__imq_cli.parse_command("shubham khandelwal")

        assert False == parse