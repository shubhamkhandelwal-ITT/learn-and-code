import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from ClientHelpers.client_executor import Client
from unittest.mock import Mock, create_autospec, patch
from ClientHelpers.client_exception import SocketErrorException, ConnectionNotCreatedException, ClientException

class TestClient(unittest.TestCase):

    __client: None

    def test_client_connect_exception(self):
        self.__client = Client()
        self.assertRaises(SocketErrorException ,self.__client.connect_to_server, Constants.HOST, Constants.PORT)
    
    def test_client_send_message_server(self):
        self.__client = Client()
        message = {}
        self.assertRaises(ClientException ,self.__client.send_message_to_server, message)

