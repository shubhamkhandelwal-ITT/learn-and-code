
import threading
import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../..")
import math
from _thread import *
from ClientHelpers.client import Client
from IMQ_common import *
from IMQ_common.constants import Constants, ImqDataFormat
from ClientHelpers.client_utility import ClientUtility
from ClientHelpers.client_executor import ClientExecutor
from ClientHelpers.client_exception import *
from ClientHelpers.constants import *
from CommandLine.imq_help import IMQ_HELP
from termcolor import colored
from CommandLine.imq_cli import IMQ_CLI
from ClientHelpers.utility import *

def run():

    cli_obj = IMQ_CLI()
    client_utility_obj = None
    user_client_socket_connection = None
    client_executor_obj = None
    connection_esatblish = False
    parse = False

    print("--- IMQ Client service ----")

    try:
        while True:
            user_input = input('>>>>>')
            try:
                parse, supported_command, args = cli_obj.parse_command(user_input)
            except ParsingFailedException as error:
                error.what()
                parse = False

            if parse:
                if CONNECT_COMMAND_KEYWORD == supported_command:
                    if not connection_esatblish:

                        try:
                            username = args[CONNECT_COMMAND_ARGUMENT]
                            client_id = create_client_id(username)
                            

                            user_client_socket_connection = Client()
                            user_client_socket_connection.connect_to_server(Constants.HOST, Constants.PORT)

                            client_utility_obj = ClientUtility(socket.gethostname(), Constants.HOST, 'v1', ImqDataFormat.JSON, client_id, username)

                            client_executor_obj = ClientExecutor(client_utility_obj, user_client_socket_connection)

                            client_socket = user_client_socket_connection.get_client_socket()

                            start_new_thread(recieve_from_server, (client_socket,))

                            connection_esatblish = client_executor_obj.execute_command[supported_command](args)

                        except ClientException as error:
                            error.what()
                    
                    else:
                        print(CONNECTION_EXIST)
                
                else:
                    if connection_esatblish == False:
                        print(colored(CONNECTION_NOT_EXIST, COLOR_RED))

                    elif supported_command in client_executor_obj.execute_command.keys():
                        client_executor_obj.execute_command[supported_command](args)

                    else:
                        raise ClientException()
            
            else:
                print(colored(NOT_VALID_COMMAND, COLOR_RED))
                IMQ_HELP.help()
    
    except:
        print(colored(CLOSE_IMQ, COLOR_RED))
        if connection_esatblish:
            client_socket.close()

if __name__ == '__main__':
    run()
