import socket
import sys
import os
sys.path.append(os.getcwd())
from IMQ_common import *
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from ClientHelpers.client_exception import ParsingFailedException
from CommandLine.client_supported_command import SupportedCommands


class IMQ_CLI:
    __command: str
    __supported_commands: list
    __help: list

    def __init__(self):
        self.__command = "imq"
        self.__supported_commands = SupportedCommands.commands

    def parse_command(self, command):
        args = {}
        parse = False
        support_command = ''

        try :
            temp_command = command.split(" ", 2)
            if temp_command[0] == self.__command:
                support_command = temp_command[1]
                for support_commands in self.__supported_commands.keys():
                    if support_command == support_commands:
                        if self.__supported_commands[support_commands] == [[]]:
                            parse = True
                            break
                        else:
                            parse, args = self.__parse_argumets(
                                temp_command[2], self.__supported_commands[support_command])
        
        except:
            raise ParsingFailedException()

        return parse, support_command, args

    def __parse_argumets(self, input_command, supported_argument):
        args = {}

        parse = False
        for arguments in supported_argument:
            total_argument = len(arguments)
            no_of_argument = 0
            for argument in arguments:
                no_of_argument += 1
                if input_command.find(argument) != 1 and input_command.split(argument)[0] == "" and total_argument == no_of_argument:
                    args[argument] = input_command.split(" ", 1)[1]
                    parse = True
                    break
                elif input_command.find(argument) != 1 and input_command.split(argument)[0] == "" and total_argument != no_of_argument:
                    if len(input_command.split(" ", 2)) == 3:
                        args[argument] = input_command.split(" ", 2)[1]
                        input_command = input_command.split(" ", 2)[2]
        return parse, args



