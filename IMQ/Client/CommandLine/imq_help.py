class IMQ_HELP:

    def help():
        print("----IMQ HELP-------")
        print("->>> imq connect -u <username>")
        print("->>> imq show -t topic")
        print("->>> imq show -m <topicname>")
        print("->>> imq status -t <topicname>")
        print("->>> imq publish -t <topicname> -m <message>")
        print("->>> imq add -t <topicname>")
