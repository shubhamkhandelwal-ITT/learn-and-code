class SupportedCommands:
    commands = {
        "connect": [["-u"]],
        "disconnect": [[]],
        "show": [["-t"], ["-m"]],
        "publish": [["-t", "-m"]],
        "status": [["-t"]],
        "add": [["-t"]]
    }