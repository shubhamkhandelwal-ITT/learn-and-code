import sqlite3

def create_database():

        conn = sqlite3.connect('IMQ.db')
        print ("Opened database successfully")

        conn.execute('''CREATE TABLE IF NOT EXISTS USER
                (user_id integer PRIMARY KEY,
                user_name Text);''')

        conn.execute('''CREATE TABLE IF NOT EXISTS TOPIC
                (topic_id integer PRIMARY KEY,
                topic_name string);''')

        conn.execute('''CREATE TABLE IF NOT EXISTS MESSAGE (
                message_id integer PRIMARY KEY,
                topic_id integer NOT NULL,
                data string,
                timestamp string,
                FOREIGN KEY(topic_id) REFERENCES TOPIC(topic_id));''')

        conn.execute('''CREATE TABLE IF NOT EXISTS DEAD_MESSAGE (
                message_id string PRIMARY KEY,
                topic_id integer NOT NULL,
                data string,
                timestamp string,
                FOREIGN KEY(topic_id) REFERENCES TOPIC(topic_id));''')

        print ("Table created successfully")

        conn.close()
