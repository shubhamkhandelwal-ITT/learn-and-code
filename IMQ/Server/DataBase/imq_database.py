import sys
import os
sys.path.append(os.getcwd())
from datetime import datetime
import sqlite3
from IMQUtility.imq_server_exception import DatabaseException
from ServerHelpers.constants import DATABASE_NAME, NO_DB_AVAILABLE

class IMQ_Database():

    __connection: None
    __cursor: None

    def __init__(self):
        try:
            self.__connection = sqlite3.connect(DATABASE_NAME)
            self.__cursor = self.__connection.cursor()
        except sqlite3.Error:
            print(NO_DB_AVAILABLE)
            raise DatabaseException

    def insert_data_in_topic_table(self, topic_id, topic_name):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute(""" INSERT into TOPIC(topic_id, topic_name) VALUES (?,?)""", (topic_id, topic_name))
            self.__connection.commit()
        else:
            raise DatabaseException

    def insert_data_in_user_table(self, user_id, user_name):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute(""" INSERT into USER(user_id, user_name) VALUES (?,?)""", (user_id, user_name))
            self.__connection.commit()
        else:
            raise DatabaseException

    def insert_data_in_message_table(self, message_id, topic_id, timestamp, data):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute(""" INSERT into MESSAGE(message_id, topic_id, data, timestamp) VALUES (?,?,?,?)""", (message_id, topic_id, data, timestamp))
            self.__connection.commit()
        else:
            raise DatabaseException

    def insert_data_in_dead_message_table(self, message_id, topic_id, timestamp, data):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute(""" INSERT into DEAD_MESSAGE(message_id, topic_id, data, timestamp) VALUES (?,?,?,?)""", (message_id, topic_id, data, timestamp))
            self.__connection.commit()
        else:
            raise DatabaseException

    def get_all_message_for_topic_id(self, topic_id):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute(""" SELECT message_id, data, timestamp from MESSAGE WHERE topic_id=?""", (topic_id,))
            result = self.__cursor.fetchall()
            return result
        else:
            raise DatabaseException

    def get_all_topic_list(self):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute("""SELECT * from TOPIC""")
            result = self.__cursor.fetchall()
            return result
        else:
            raise DatabaseException

    def get_all_user_list(self):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute("""SELECT * from USER""")
            result = self.__cursor.fetchall()
            return result
        else:
            raise DatabaseException

    def check_if_user_exist(self, user_name):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute("""SELECT user_id from USER WHERE user_name=?""", (user_name))
            result = self.__cursor.fetchall()
            return result
        else:
            raise DatabaseException

    def delete_message_from_database(self, message_id):
        if self.__connection != None and self.__cursor != None:
            self.__cursor.execute(""" DELETE from MESSAGE WHERE message_id=?""", (message_id,))
            result = self.__cursor.fetchall()
            return result
        else:
            raise DatabaseException
    
    def __del__(self):
        if self.__connection != None:
            self.__connection.close()