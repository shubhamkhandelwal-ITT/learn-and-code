import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())
from unittest.mock import Mock, create_autospec, patch
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from IMQUtility.server_container import ServerDataLoader
from DataBase.imq_database import IMQ_Database



class TestServerDataLoader(unittest.TestCase):

    __server_data: None
    mock_database_object = create_autospec(IMQ_Database)
    mock_database_object.get_all_topic_list.return_value = [(111, 'Abundance'), (8843, 'Love')]
    mock_database_object.get_all_user_list.return_value =[(121, 'username')]
    mock_database_object.get_all_message_for_topic_id.return_value = (111, 'message', '22:00:12')

    def test_server_data_loader_dict_of_topic(self):
        self.__server_data = ServerDataLoader(self.mock_database_object)
        actual_result = {
            'Abundance': 111,
            'Love': 8843
        }
        expect_result = self.__server_data.dict_of_topic

        assert actual_result.keys() == expect_result.keys()
    
    def test_server_data_loader_dict_of_user(self):
        self.__server_data = ServerDataLoader(self.mock_database_object)
        actual_result = {
            'username': 121
        }
        expect_result = self.__server_data.dict_of_user

        assert actual_result.keys() == expect_result.keys()
    
    def test_server_data_loader_list_of_queue(self):
        self.__server_data = ServerDataLoader(self.mock_database_object)
        expect_list_of_queue = self.__server_data.list_of_queue
        
        assert 'Abundance' == expect_list_of_queue[0].get_topic_name()
        assert 'Love' == expect_list_of_queue[1].get_topic_name()

    def test_server_data_loader_list_of_queue_topic_list_empty(self):
        mock_database_object = create_autospec(IMQ_Database)
        mock_database_object.get_all_topic_list.return_value = []
        mock_database_object.get_all_user_list.return_value =[]

        self.__server_data = ServerDataLoader(mock_database_object)
        actual_result = {
            'username': 121
        }
        expect_list_of_queue = self.__server_data.list_of_queue

        assert 0 == len(expect_list_of_queue)
    
    def test_server_data_loader_dict_of_topic_topic_list_empty(self):
        mock_database_object = create_autospec(IMQ_Database)
        mock_database_object.get_all_topic_list.return_value = []
        mock_database_object.get_all_user_list.return_value =[]
        self.__server_data = ServerDataLoader(mock_database_object)

        expect_dict_of_topic = self.__server_data.dict_of_topic

        assert 0 == len(expect_dict_of_topic.keys())
    
    def test_server_data_loader_dict_of_user_topic_list_empty(self):
        mock_database_object = create_autospec(IMQ_Database)
        mock_database_object.get_all_topic_list.return_value = []
        mock_database_object.get_all_user_list.return_value =[]
        self.__server_data = ServerDataLoader(mock_database_object)

        expect_dict_of_topic = self.__server_data.dict_of_topic

        assert 0 == len(expect_dict_of_topic.keys())
