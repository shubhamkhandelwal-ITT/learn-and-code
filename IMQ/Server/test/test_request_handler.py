import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())
from unittest.mock import Mock, create_autospec, patch
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from ServerHelpers.client_handler import ClientHandler
from DataBase.imq_database import IMQ_Database
from IMQUtility.server_utility import ServerUtility
from IMQUtility.server_container import ServerDataLoader
from ServerHelpers.imq_request_service import RequestService
from ServerHelpers.imq_requset_handler import RequestHandler
from IMQUtility.imq_queue import Imq_queue
from IMQUtility.imq_server_exception import ArgumentInvalidException


class TestRequestHandler(unittest.TestCase):

    mock_request_service = create_autospec(RequestService)

    __request_handler: None

    def test_request_handler_topic_list(self):
        request = {}
        topic_list = lambda : {'message': 'No Topic Exist'}
        request['topic_list'] = topic_list
        self.mock_request_service.requests = request

        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {}
        }
        status, acknowledgement = self.__request_handler.requests['topic_list'](data)
        actual_result = {}
        actual_result['message'] = 'No Topic Exist'
        assert status == True
        assert actual_result['message'] == acknowledgement['message']
    
    def test_request_handler_add_topic(self):
        request = {}
        topic_list = lambda topic_name: 'Topic Created Successfully'
        request['add_topic'] = topic_list
        self.mock_request_service.requests = request

        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'topic_name': 'topic'}
        }
        status, acknowledgement = self.__request_handler.requests['add_topic'](data)
        actual_result = {}
        actual_result['message'] = 'Topic Created Successfully'
        assert status == True
        assert actual_result['message'] == acknowledgement['message']
    
    def test_request_handler_status_topic(self):
        request = {}
        topic_list = lambda topic_name: {'no_of_message': 1}
        request['status_of_topic'] = topic_list
        self.mock_request_service.requests = request

        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'topic_name': 'topic'}
        }
        status, acknowledgement = self.__request_handler.requests['status_of_topic'](data)
        actual_result = {}
        actual_result['no_of_message'] = 1
        assert status == True
        assert actual_result['no_of_message'] == acknowledgement['no_of_message']
    
    def test_request_handler_publish_topic(self):
        request = {}
        topic_list = lambda topic_name, publish_message, message_id, timestamp: 'message is love'
        request['publish_topic_data'] = topic_list
        self.mock_request_service.requests = request

        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'topic_name': 'topic', 'publish_message': 'message is love'},
            'id': 'asdf',
            'timestamp': '22:00:10'
        }
        status, acknowledgement = self.__request_handler.requests['publish_topic_data'](data)
        actual_result = {}
        actual_result['message'] = 'message is love'
        assert status == True
        assert actual_result['message'] == acknowledgement['message']
    
    def test_request_handler_push_topic_data(self):
        request = {}
        topic_list = lambda topic_name :{ 'message': 'message is love'}
        request['pull_topic_data'] = topic_list
        self.mock_request_service.requests = request

        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'topic_name': 'topic'},
            'id': 'asdf',
            'timestamp': '22:00:10'
        }
        status, acknowledgement = self.__request_handler.requests['pull_topic_data'](data)
        actual_result = {}
        actual_result['message'] = 'message is love'
        assert status == True
        assert actual_result['message'] == acknowledgement['message']
    
    def test_request_handler_topic_list_invalid_argument(self):
        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'a': 'invalid'}
        }
        self.assertRaises(ArgumentInvalidException, self.__request_handler.requests['topic_list'], data)
    
    def test_request_handler_pull_topic_data_invalid_argument(self):
        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'a': 'invalid'}
        }
        self.assertRaises(ArgumentInvalidException, self.__request_handler.requests['pull_topic_data'], data)
    
    def test_request_handler_publish_topic_invalid_argument(self):
        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'a': 'invalid'}
        }
        self.assertRaises(ArgumentInvalidException, self.__request_handler.requests['publish_topic_data'], data)

    def test_request_handler_status_of_topic_invalid_argument(self):
        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'a': 'invalid'}
        }
        self.assertRaises(ArgumentInvalidException, self.__request_handler.requests['status_of_topic'], data)

    def test_request_handler_add_topic_invalid_argument(self):
        self.__request_handler = RequestHandler(self.mock_request_service)
        data = {
            'data': {'a': 'invalid'}
        }
        self.assertRaises(ArgumentInvalidException, self.__request_handler.requests['add_topic'], data)
