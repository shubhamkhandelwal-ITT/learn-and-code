import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())
from unittest.mock import Mock, create_autospec, patch
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from ServerHelpers.client_handler import ClientHandler
from DataBase.imq_database import IMQ_Database
from IMQUtility.server_utility import ServerUtility
from IMQUtility.server_container import ServerDataLoader
from ServerHelpers.imq_request_service import RequestService
from IMQUtility.imq_queue import Imq_queue


class TestRequestService(unittest.TestCase):

    mock_server_data = create_autospec(ServerDataLoader)
    mock_server_data.dict_of_topic = {'topic':1}
    mock_server_data.list_of_queue = []
    
    mock_database_object = create_autospec(IMQ_Database)
    mock_database_object.insert_data_in_message_table.return_value = True
    mock_database_object.insert_data_in_topic_table.return_value = True

    __request_service: None

    def test_request_service_add_exist_topic(self):
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['add_topic']('topic')
        assert 'Topic Already Present' == result
    
    def test_request_service_add_new_topic(self):
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['add_topic']('ravi')
        assert 'Topic Created Successfully' == result

    def test_request_service_topic_list(self):
        actual_result = {}
        self.mock_database_object.get_all_topic_list.return_value = ['topic']
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['topic_list']()
        actual_result['message'] = ['topic']
        assert actual_result['message'] == result['message']
    
    def test_request_service_topic_list_empty(self):

        actual_result = {}
        self.mock_server_data.dict_of_topic = {}
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['topic_list']()
        actual_result['message'] = 'No Topic List Exist'
        assert actual_result['message'] == result['message']
    
    def test_request_service_publish_topic(self):
        actual_result = {}
        imq_object = Imq_queue('topic')
        list_of_queue = []
        list_of_queue.append(imq_object)
        self.mock_server_data.list_of_queue = list_of_queue
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['publish_topic_data']('topic', 'Love yourself', 'abcd', '22:02:033')
        actual_result['message'] = 'Message Publish Successfully'
        assert actual_result['message'] == result['message']
    
    def test_request_service_publish_topic_not_exist(self):
        actual_result = {}
        imq_object = Imq_queue('topic')
        list_of_queue = []
        list_of_queue.append(imq_object)
        self.mock_server_data.list_of_queue = list_of_queue
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['publish_topic_data']('love', 'Love yourself', 'abcd', '22:02:033')
        actual_result['message'] = 'No Topic Exist'
        assert actual_result['message'] == result['message']
    
    def test_request_service_pull_topic_data(self):

        actual_result = {}
        imq_object = Imq_queue('topic')
        message = (123, 'Love yourself', '22:02:033')
        imq_object.add_message(message)
        list_of_queue = []
        list_of_queue.append(imq_object)
        self.mock_server_data.list_of_queue = list_of_queue

        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['pull_topic_data']('topic')
        actual_result['message'] = [message]
        assert actual_result['message'] == result['message']
    
    def test_request_service_pull_topic_data_not_exist(self):
        actual_result = {}
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['pull_topic_data']('devraj')
        actual_result['message'] = 'No Topic Exist'
        assert actual_result['message'] == result['message']
    
    def test_request_service_status_of_topic(self):
        actual_result = {}
        imq_object = Imq_queue('topic')
        message = (123, 'Love yourself', '22:02:033')
        imq_object.add_message(message)
        list_of_queue = []
        list_of_queue.append(imq_object)
        self.mock_server_data.list_of_queue = list_of_queue

        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['status_of_topic']('topic')
        actual_result['no_of_message'] = 1
        assert actual_result['no_of_message'] == result['no_of_message']
    
    def test_request_service_status_of_topic_not_exist(self):
        actual_result = {}
        self.__request_service = RequestService(self.mock_server_data, self.mock_database_object)
        result = self.__request_service.requests['status_of_topic']('devraj')
        actual_result['message'] = 'No Topic Exist'
        assert actual_result['message'] == result['message']


    
    
