import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../..")
print(sys.path)
from test_request_service import *
from test_request_handler import *
from test_server_container import *
from test_imq_queue import *
from test_server_utility import *

if __name__ == '__main__':
    unittest.main()