import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())
from unittest.mock import Mock, create_autospec, patch
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from IMQUtility.imq_queue import Imq_queue



class TestImqQueue(unittest.TestCase):

    __imq_queue: None

    def test_imq_queue_get_topic_name(self):
        self.__imq_queue = Imq_queue('topic')
        result = self.__imq_queue.get_topic_name()
        assert 'topic' == result

    def test_imq_queue_add_message(self):
        self.__imq_queue = Imq_queue('topic')
        self.__imq_queue.add_message('shubham khandelwal')
        actual_result = ['shubham khandelwal']
        expect_result =self.__imq_queue.get_messages()
        assert actual_result == expect_result
    
    def test_imq_queue_extend_message(self):
        self.__imq_queue = Imq_queue('topic')
        self.__imq_queue.add_message('shubham khandelwal')
        list_extend = ['ravi']
        self.__imq_queue.extends_message(list_extend)
        actual_result = ['shubham khandelwal', 'ravi']
        expect_result =self.__imq_queue.get_messages()
        assert actual_result == expect_result

    def test_imq_queue_erase_message(self):
        self.__imq_queue = Imq_queue('topic')
        self.__imq_queue.add_message('shubham khandelwal')
        self.__imq_queue.erase_message()
        actual_result = []
        expect_result =self.__imq_queue.get_messages()
        assert actual_result == expect_result
