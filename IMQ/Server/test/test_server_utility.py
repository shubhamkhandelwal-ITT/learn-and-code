import unittest
import sys
import os
import socket
sys.path.append(os.getcwd())
from IMQUtility.server_utility import ServerUtility
from IMQ_common.constants import ImqDataFormat, ResponseType, Constants

class TestServerUtility(unittest.TestCase):


    __server_utility: None
    def test_request_object_example(self):
        server_id = 123
        data = {}
        self.__server_utility = ServerUtility(socket.gethostname(), Constants.HOST, 'v1', ImqDataFormat.JSON, server_id)
        response_object = self.__server_utility.create_response(ResponseType.SUCCESS, data)

        assert server_id == response_object.client_id
        assert data == response_object.data
    
    def test_request_object_example_2(self):
        server_id = 124
        data = {}
        self.__server_utility = ServerUtility(socket.gethostname(), Constants.HOST, 'v1', ImqDataFormat.JSON, server_id)
        response_object = self.__server_utility.create_response(ResponseType.FAIL, data)

        assert server_id == response_object.client_id
        assert data ==response_object.data
        assert ResponseType.FAIL.value == response_object.response_type
