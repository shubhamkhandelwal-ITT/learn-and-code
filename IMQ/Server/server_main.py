import threading
import sys
import os
import json
import datetime
import time
import socket
sys.path.append(os.path.abspath(__file__) + "/../..")
from _thread import *
from ServerHelpers.server import Server
from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from DataBase.imq_database import IMQ_Database
from ServerHelpers.client_handler import ClientHandler
from ServerHelpers.imq_request_service import RequestService
from ServerHelpers.imq_requset_handler import RequestHandler
from ServerHelpers.imq_response_handler import ResponseHandler
from IMQUtility.imq_queue import Imq_queue
from IMQUtility.server_container import ServerDataLoader
from IMQUtility.server_utility import ServerUtility
from IMQUtility.imq_server_exception import *
from ServerHelpers.constants import*
from termcolor import colored

print_lock = threading.Lock()


def check_dead_message(server_utility):

    database_object = IMQ_Database()
    while True:

        now_time = datetime.datetime.now()

        try:
            print_lock.acquire()
            for message_queue in server_utility.list_of_queue:

                temp_message = []
                topic_id = server_utility.dict_of_topic[message_queue.get_topic_name()]

                for message in message_queue.get_messages():
                    if len(message) != 0:
                        queueMessageTimeStamp = datetime.datetime.strptime(
                            message[2], DATE_TIME_FORMAT)
                        timeDifference = round(
                            (now_time - queueMessageTimeStamp).total_seconds())
                        if timeDifference > DEAD_MESSAGE_TIME:
                            temp_message.append(message)
                        else:
                            print(DEAD_MESSAGE_LOG, message[0])
                            database_object.delete_message_from_database(message[0])
                            database_object.insert_data_in_dead_message_table(message[0], topic_id, message[2], message[1])
                            
                message_queue.erase_message()
                message_queue.extends_message(temp_message)
        
        except:
            print_lock.release()
            print(EXCEPTION_IN_DEAD_MESSAGE)
            break

        print_lock.release()
        time.sleep(SLEEP_THREAD_TIME)
    

# thread function
def start_client(client_socket, server_utility_obj, server_utility):

    try:

        database_object = IMQ_Database()

        request_service = RequestService(server_utility, database_object)
        response_handler = ResponseHandler(client_socket, server_utility_obj)
        request_handler = RequestHandler(request_service)

        client_handler = ClientHandler(request_handler, response_handler)
        while True:

            # data received from client
            data = client_socket.recv(RECIVING_BUFFER_SIZE)

            # if no data recieve close
            if not data:
                break

            # convert into string
            data = data.decode(ENCODE_TYPE)
            data = json.loads(data)
            print(CLIENT_REQUEST)
            print(colored(data, COLOR_YELLOW))

            print_lock.acquire()
            try:
                client_handler.handle(data)
            except ServerException as error:
                error.what()
            print_lock.release()

    except:
        print(CLIENT_CONNECTION_CLOSED)
        client_socket.close()
        if print_lock.locked():
            print_lock.release()


def run():
    main_server = None
    server_data = None
    server_utility_obj = None
    database_object = None

    try:

        # Intialize main server
        main_server = Server()
        main_server.bind_server(Constants.HOST, Constants.PORT)
        main_server.start_listening(NO_CONNECTION_LISTEN)

        # Intialize server utility object
        server_utility_obj = ServerUtility(Constants.PORT, socket.gethostname(), PROTOCOL_VERIOSN, ImqDataFormat.JSON, SERVER_ID)

        try:
            database_object = IMQ_Database()
        except DatabaseException as error:
            error.what()
            error.handle_exception()
            database_object = IMQ_Database()

        # Load Server Data 
        try:
            server_data = ServerDataLoader(database_object)

        except ServerDataLoaderException as error:
            error.what()
            raise ServerException()

        start_new_thread(check_dead_message, (server_data,))
        
        # Start loop
        while True:

            # Accept connection from client
            client_socket, addr = main_server.accept_connection()

            try:
                # Handle Client connection request
                data = client_socket.recv(RECIVING_BUFFER_SIZE)
                data = data.decode(ENCODE_TYPE)
                data = json.loads(data)
                if data[REQUEST_TYPE_KEY] == RequestType.SETUP_CONNECTION.value:

                    if data[CLIENT_NAME_KEY] in server_data.dict_of_user.keys():

                        print(EXISTED_USER, data[CLIENT_NAME_KEY])
                    else:

                        print(NEW_CLIENT, data[CLIENT_NAME_KEY])

                        database_object.insert_data_in_user_table(data[CLIENT_ID_KEY], data[CLIENT_NAME_KEY])

                    # Start a new thread and return its identifier
                    start_new_thread(start_client, (client_socket, server_utility_obj, server_data))
                else:
                    print(UNKNOW_CLIENT)
                    client_socket.close()
            
            except:
                client_socket.close()
                print(CLIENT_CONNECTION_CLOSED, addr[0], ':', addr[1])

        main_server.close()

    except ServerException as error:
        error.what()


if __name__ == '__main__':
    run()
