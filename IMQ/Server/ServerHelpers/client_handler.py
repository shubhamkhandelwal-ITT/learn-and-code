import sys
import os
sys.path.append(os.getcwd())
from IMQ_common.constants import Constants, ImqDataFormat, ResponseType
from DataBase.imq_database import IMQ_Database
from ServerHelpers.imq_request_service import RequestService
from ServerHelpers.imq_requset_handler import RequestHandler
from IMQUtility.imq_server_exception import ArgumentInvalidException, ServerException
from ServerHelpers.imq_response_handler import ResponseHandler
from ServerHelpers.constants import REQUEST_TYPE_KEY, EXCEPTION_KEY,EXCEPTION_KEY_VALUE

class ClientHandler:
    __request_handler: None
    __response_handler: None

    def __init__(self, request_handler, response_handler):
        self.__request_handler = request_handler
        self.__response_handler = response_handler

    def handle(self, data):

        try:
            request_type = data[REQUEST_TYPE_KEY]
            status = False
            acknowledgment = {}
            try:
                status, acknowledgment = self.__request_handler.requests[request_type](data)
            
            except ArgumentInvalidException as error:
                error.what()
                acknowledgment[EXCEPTION_KEY] = EXCEPTION_KEY_VALUE

            self.__response_handler.acknowledge_to_client(status, acknowledgment)

        except:
            raise ServerException()








