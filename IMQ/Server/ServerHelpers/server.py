import socket 
from IMQUtility.imq_server_exception import SocketErrorException
from ServerHelpers.constants import SOCKET_NOT_CREATED

class Server:
    _server_socket = None
    def __init__(self):
        try:
            self._server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as error_message:
            print(SOCKET_NOT_CREATED + str(error_message))
            raise SocketErrorException()
		 
    def bind_server(self, host, port):
        self._server_socket.bind((host, port))

    def start_listening(self, number):
        try:
            self._server_socket.listen(number)
        except socket.error as error_message:
            print(SOCKET_NOT_CREATED + str(error_message))
            raise SocketErrorException()

    def accept_connection(self):
        try:
            client_socket, addr = self._server_socket.accept()
        except socket.error as error_message:
            print(SOCKET_NOT_CREATED+ str(error_message))
            raise SocketErrorException()
        
        return client_socket, addr

    def close(self):
        self._server_socket.close()
