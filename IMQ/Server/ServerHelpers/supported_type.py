class RequestArgument:
    request_argument = {
        'add_topic': ['topic_name'],
        'publish_topic_data': ['topic_name', 'publish_message'],
        'status_of_topic': ['topic_name'],
        'topic_list': [],
        'pull_topic_data': ['topic_name'],
        'close': []
    }