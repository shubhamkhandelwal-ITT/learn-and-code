from IMQ_common.constants import Constants, ImqDataFormat, RequestType
from ServerHelpers.supported_type import RequestArgument
from IMQUtility.imq_server_exception import ArgumentInvalidException
from ServerHelpers.constants import *

class RequestHandler:
    requests: dict
    __request_argument: None
    __request_service: None

    def __init__(self, request_service):
        self.requests = self.__load_requests()
        self.__request_argument = RequestArgument.request_argument
        self.__request_service = request_service

    def __add_topic(self, data):
        status = False
        acknowledgment = {}
        if self.__validate_request_argument(RequestType.ADD_TOPIC.value, data[DATA_KEY].keys()):
            topic_name = data[DATA_KEY][TOPIC_NAME_DATA_KEY]
            acknowledgment[MESSAGE_KEY] = self.__request_service.requests[RequestType.ADD_TOPIC.value](topic_name)
            status = True
        
        else:
            raise ArgumentInvalidException()
            
        return status, acknowledgment


    
    def __publish_topic(self, data):
        status = False
        acknowledgment = {}
        if self.__validate_request_argument(RequestType.PUBLISH_TOPIC_DATA.value, data[DATA_KEY].keys()):
            topic_name = data[DATA_KEY][TOPIC_NAME_DATA_KEY]
            publish_message = data[DATA_KEY][PUBLISH_DATA_KEY]
            
            acknowledgment[MESSAGE_KEY] = self.__request_service.requests[
                RequestType.PUBLISH_TOPIC_DATA.value](topic_name, publish_message, data[DATA_ID], data[TIMESTAMP_KEY])
                
            status = True
        
        else:
            raise ArgumentInvalidException()
        
        return status, acknowledgment

    def __topic_list(self, data):
        status = False
        acknowledgment = {}
        if self.__validate_request_argument(RequestType.TOPIC_LIST.value, data[DATA_KEY].keys()):
            acknowledgment = self.__request_service.requests[
                RequestType.TOPIC_LIST.value]()
                
            status = True
        
        else:
            raise ArgumentInvalidException()
        
        return status, acknowledgment

    def __status_of_topic(self, data):
        status = False
        acknowledgment = {}
        if self.__validate_request_argument(RequestType.STATUS_OF_TOPIC.value, data[DATA_KEY].keys()):
            topic_name = data[DATA_KEY][TOPIC_NAME_DATA_KEY]
            acknowledgment = self.__request_service.requests[
                RequestType.STATUS_OF_TOPIC.value](topic_name) 
            status = True
        
        else:
            raise ArgumentInvalidException()
        
        return status, acknowledgment
    
    def __pull_topic_data(self, data):
        status = False
        acknowledgment = {}
        if self.__validate_request_argument(RequestType.PULL_TOPIC_DATA.value, data[DATA_KEY].keys()):
            topic_name = data[DATA_KEY][TOPIC_NAME_DATA_KEY]
            acknowledgment = self.__request_service.requests[
                RequestType.PULL_TOPIC_DATA.value](topic_name)
                
            status = True
        
        else:
            raise ArgumentInvalidException()
        
        return status, acknowledgment
            

    def __load_requests(self):
        request = {}
        request[RequestType.ADD_TOPIC.value] = self.__add_topic
        request[RequestType.PUBLISH_TOPIC_DATA.value] = self.__publish_topic
        request[RequestType.STATUS_OF_TOPIC.value] = self.__status_of_topic
        request[RequestType.TOPIC_LIST.value] = self.__topic_list
        request[RequestType.PULL_TOPIC_DATA.value] = self.__pull_topic_data

        return request
    
    def __validate_request_argument(self, request_type, args: list):
        supported_argument = self.__request_argument[request_type]
        supported = True
        if len(args) == len(supported_argument):
            for arg in args:
                if arg not in supported_argument:
                    supported = False
        else:
            supported = False
        
        return supported