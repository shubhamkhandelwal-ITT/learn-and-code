from IMQ_common.constants import Constants, ImqDataFormat, ResponseType
from IMQUtility.imq_server_exception import ResponseHandlerException
from ServerHelpers.constants import ENCODE_TYPE, COLOR_BLUE
from termcolor import colored


class ResponseHandler:

    __client_socket: None
    __server_utility_object: None

    def __init__(self, client_socket, server_utility_obj):
        self.__client_socket = client_socket
        self.__server_utility_object = server_utility_obj
    
    def acknowledge_to_client(self, status: bool, acknowledgment: dict):

        try:
            response_type = ResponseType.SUCCESS if status else ResponseType.FAIL
            response_message = self.__server_utility_object.create_response(response_type, acknowledgment)
            response_json = self.__server_utility_object.convert_object_into_json(response_message)
            print(colored(response_json, COLOR_BLUE))
            self.__client_socket.send(response_json.encode(ENCODE_TYPE))
        
        except: 
            raise ResponseHandlerException()