import math
import uuid
from IMQUtility.imq_queue import Imq_queue
from ServerHelpers.constants import *
from IMQ_common.constants import RequestType

class RequestService:
    requests: dict
    __server_data: None
    __database_object: None

    def __init__(self, server_data, database_object):
        self.__server_data = server_data
        self.__database_object = database_object
        self.requests = self.__load_requests()

    def __create_unique_id(self, name):
        unique_id= int(uuid.uuid5(uuid.NAMESPACE_DNS, name))
        uid = round(math.log(unique_id) * 100)

        return uid


    def __add_topic(self, topic_name):

        if topic_name not in self.__server_data.dict_of_topic.keys():
        
            topic_id = self.__create_unique_id(topic_name)

            self.__database_object.insert_data_in_topic_table(topic_id, topic_name)

            new_message_queue = Imq_queue(topic_name)
            self.__server_data.list_of_queue.append(new_message_queue)
            self.__server_data.dict_of_topic[topic_name] = topic_id

            return TOPIC_CREATED
        
        else:

            return TOPIC_ALREADY_PRESENT
    
    def __publish_topic(self, topic_name, publish_message, message_id, timestamp):

        data = {}
        if topic_name in self.__server_data.dict_of_topic.keys():

            uid = self.__create_unique_id(message_id)
            message = (uid, publish_message, timestamp)

            for list1 in self.__server_data.list_of_queue:
                if topic_name == list1.get_topic_name():
                    list1.add_message(message)
            
            topic_id = self.__server_data.dict_of_topic[topic_name]
            self.__database_object.insert_data_in_message_table(uid, topic_id, timestamp, publish_message)

            data[MESSAGE_KEY] = MESSAGE_PUBLISH_SUCCESSFULLY
        
        else:
            data[MESSAGE_KEY] = NO_TOPIC_EXIST
        
        return data

    def __pull_topic_data(self, topic_name):
        data={}
        if topic_name in self.__server_data.dict_of_topic.keys():
            for list1 in self.__server_data.list_of_queue:
                if topic_name == list1.get_topic_name():
                    data[MESSAGE_KEY] = list1.get_messages()
        else:
            data[MESSAGE_KEY] = NO_TOPIC_EXIST
        
        return data
    
    def __status_of_topic(self, topic_name):

        data = {}
        if topic_name in self.__server_data.dict_of_topic.keys():
            for list1 in self.__server_data.list_of_queue:
                if topic_name == list1.get_topic_name():
                    data[NO_OF_MESSAGE_KEY] = len(list1.get_messages())
        
        else:
            data[MESSAGE_KEY] = NO_TOPIC_EXIST
        
        return data
    
    def __topic_list(self):
        data = {}
        if len(self.__server_data.dict_of_topic.keys()) != 0:
            topic_list = self.__database_object.get_all_topic_list()
            data[MESSAGE_KEY] = topic_list
        else:
            data[MESSAGE_KEY] = NO_TOPIC_LIST_EXIST
        
        return data


    def __load_requests(self):
        request = {}
        request[RequestType.ADD_TOPIC.value] = self.__add_topic
        request[RequestType.PUBLISH_TOPIC_DATA.value] = self.__publish_topic
        request[RequestType.STATUS_OF_TOPIC.value] = self.__status_of_topic
        request[RequestType.TOPIC_LIST.value] = self.__topic_list
        request[RequestType.PULL_TOPIC_DATA.value] = self.__pull_topic_data

        return request

