from datetime import datetime
import sys
import json
from IMQ_common import *
from IMQ_common.imq_protocol import ImqRequest, ImqResponse
from IMQ_common.constants import ImqDataFormat, ResponseType, RequestType

class ServerUtility:
    _source_url: None
    _destination_url: None
    _version: None
    _data_format: None
    _server_id: None

    def __init__(self, source_url, destination_url , version, data_format: ImqDataFormat, client_id):
        self._source_url = source_url
        self._destination_url = destination_url
        self._version = version
        self._data_format = data_format.value
        self.server_id = client_id

    def create_response(self, response_type: ResponseType, data: str):
        response = ImqResponse()
        response.data = data
        response.data_format = self._data_format
        response.source_url = self._source_url
        response.destination_url = self._destination_url
        response.set_unique_id()
        response.response_type = response_type.value
        response.version = self._version
        response.client_id = self.server_id
        response.timestamp = str(datetime.now())

        return response

    def convert_object_into_json(self, message):
        json_obj = json.dumps(message.__dict__)
        return json_obj
