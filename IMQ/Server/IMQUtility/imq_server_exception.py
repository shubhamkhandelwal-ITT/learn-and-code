from DataBase.database import create_database


class ServerException(Exception):

    def what(self):
        pass

class ArgumentInvalidException(ServerException):
    error_message = 'Invalid argument'
    
    def what(self):
        print(self.error_message)

class DatabaseException(ServerException):
    error_message = 'Database Failed'

    def what(self):
        print(self.error_message)
    
    def handle_exception(self):
        create_database()

class SocketErrorException(ServerException):
    error_message = 'Socket Error Exception'

    def what(self):
        print(self.error_message)

class ResponseHandlerException(ServerException):
    error_message = 'Response Handler Exception'

    def what(self):
        print(self.error_message)


class ConnectionClosedException(ServerException):
    error_message = 'Connection closed'

    def what(self):
        print(self.error_message)

class ServerDataLoaderException(DatabaseException):
    error_message = 'Server Database Loading Failed'

    def what(self):
        print(self.error_message)
