
class Imq_queue:
    __topic_name: str
    __message: list

    def __init__(self, topic_name):
        self.__topic_name = topic_name
        self.__message = []

    def get_topic_name(self):
        return self.__topic_name

    def get_messages(self):
        return self.__message

    def add_message(self, message):
        self.__message.append(message)
    
    def extends_message(self, messages):
        self.__message.extend(messages)

    def erase_message(self):
        self.__message = []

