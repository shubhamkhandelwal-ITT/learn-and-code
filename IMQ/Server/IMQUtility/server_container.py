from DataBase.imq_database import IMQ_Database
from IMQUtility.imq_queue import Imq_queue
from IMQUtility.imq_server_exception import DatabaseException, ServerDataLoaderException
from ServerHelpers.constants import MESSAGE_AVAILABLE, USER_AVAILABLE, TOPIC_AVAILABLE


class ServerDataLoader:

    list_of_queue: None
    dict_of_user: None
    dict_of_topic: None
    __databaseObject: None

    def __init__(self, database_object):
        try:
            self.list_of_queue = []
            self.dict_of_user = {}
            self.dict_of_topic= {}
            self.__databaseObject = database_object
            self.__load_topic_and_queue()
            self.__load_user_details_from_data_base()
            self.__load_topic_details_from_data_base()

        except:
            raise ServerDataLoaderException()

    def __load_topic_and_queue(self):

        if self.__databaseObject != None:
            topic_list = self.__databaseObject.get_all_topic_list()

            if(len(topic_list) != 0):

                for topic in topic_list:
                    print(topic)
                    #Create Imq_queue
                    queue = Imq_queue(topic[1])
                    messages_list = self.__databaseObject.get_all_message_for_topic_id(topic[0])

                    print(MESSAGE_AVAILABLE)
                    print(topic[1])
                    print(messages_list)

                    queue.extends_message(messages_list)
                    self.list_of_queue.append(queue)

    def __load_user_details_from_data_base(self):

        if self.__databaseObject != None:
            user_list = self.__databaseObject.get_all_user_list()

            print(USER_AVAILABLE)
            print(user_list)

            if len(user_list) != 0:
                for user in user_list:
                    self.dict_of_user[user[1]] = user[0]
    
    def __load_topic_details_from_data_base(self):

        if self.__databaseObject != None:
            topic_list = self.__databaseObject.get_all_topic_list()

            print(TOPIC_AVAILABLE)
            print(topic_list)

            if len(topic_list) != 0:
                for topic in topic_list:
                    self.dict_of_topic[topic[1]] = topic[0]


    def get_list_of_queue(self):
        return self.list_of_queue
    
    def get_dict_of_user(self):
        return self.dict_of_user
    





