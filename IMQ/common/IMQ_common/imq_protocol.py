""" File: imq_protocol.py

Provide IMQ Protocol class
"""

import uuid

class ImqProtocol:

    data: dict
    timestamp: None
    id: None
    source_url: str
    destination_url: str
    data_format: str
    version: str
    client_id: int
    client_name: str

    def set_unique_id(self):
        self.id = str(uuid.uuid4())

class ImqRequest(ImqProtocol):

    request_type: str

class ImqResponse(ImqProtocol):

    response_type: str

