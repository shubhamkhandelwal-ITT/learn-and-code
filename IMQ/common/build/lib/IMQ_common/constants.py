from enum import Enum

class Constants:
    HOST = "127.0.0.1"
    PORT = 8081

class SpecialCommands:
    SEPERATOR = '+'
    SHOW_USER = '@available_user'
    CLOSE = '@exit'

class ImqDataFormat(Enum):
    JSON = 'json'
    XML = 'XML'

class RequestType(Enum):
    CONNECT_TO_TOPIC = 'connect_to_topic'
    SUBSCRIBE_TO_TOPIC = 'subscribe_to_topic'
    PUBLISH_TOPIC_DATA = 'publish_topic_data'
    STATUS_OF_TOPIC = 'status_of_topic'
    TOPIC_LIST = 'topic_list'
    ADD_TOPIC = 'add_topic'
    PULL_TOPIC_DATA = 'pull_topic_data'
    SETUP_CONNECTION = 'setup_connection'
    ECHO = 'echo'
    CLOSE = 'close'

class ResponseType(Enum):
    SUCCESS = 'success'
    FAIL = 'fail'

