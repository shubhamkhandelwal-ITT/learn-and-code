from setuptools import find_packages, setup

setup(
    name="IMQ_common",
    packages=find_packages(include=["IMQ_common"]),
    version="0.1.0",
    description="My first Python library",
    author="Me",
    license="MIT",
)