import requests
import json
import sys

def main ():
    blog_name = get_blog_name ()
    start,end = get_post_range ()
    blog_url = get_blog_url (blog_name)
    
    blog_data = get_blog_data (blog_url, 0, 0)
    display_basic_blog_information (blog_data)

    photo_urls = get_photo_urls (start, end, blog_url)
    display_photo_urls (photo_urls)


def get_blog_name ():
    blog_name = input ("Enter the tumblr blog name ")
    return blog_name


def get_blog_url (blog_name):
    blog_url = BLOG_URL_PREFIX + blog_name + BLOG_URL_SUFFIX
    print (blog_url)
    return blog_url


def get_post_range ():
    start, end=input ("Enter the range(start from 0)").split ('-')
    return start, end


def get_blog_data (blog_url, start, end):
    params = get_api_parameter ('photo', start, end)
    response = get_api_response (blog_url, params)
    blog_data = convert_response_into_json (response)
    return blog_data


def get_api_parameter (data_type, start, end):
    start = int (start)
    end = int (end)

    number_of_post = end - start + 1
    parameter = {'type': data_type, 'num': number_of_post, 'start': start}
    return parameter


def get_api_response (blog_url, params):
    response=requests.get (blog_url, params)
    if response.status_code != 200 :
      sys.exit (ERROR_INVALID)
    return response


def convert_response_into_json (response):
    blog_data = response.content.decode ("utf-8")
    blog_data = blog_data [22:-2]# remove noise from data
    blog_data = json.loads (blog_data)
    return blog_data


def get_number_of_iteration_of_blog_url (start, end):
    temp = start
    n_times = 0
    while temp < end:
        n_times += 1
        temp += BLOG_END_LIMIT
    return n_times


def display_basic_blog_information (blog_data):
    print ("Title-", blog_data['tumblelog']['title'])
    print ('name-', blog_data['tumblelog']['name'])
    print ('Description-', blog_data['tumblelog']['description'])
    print ('No of post-', blog_data['posts-total'])


def display_photo_urls (photo_urls):
    photo_no = 1
    for photo_url in photo_urls:
        print (photo_no,photo_url)
        photo_no += 1


def get_photo_urls (start,end,blog_url):
    photo_urls = []
    start = int (start)
    end = int (end)
    n_times = get_number_of_iteration_of_blog_url (start, end)

    if end - start <= BLOG_END_LIMIT:# set temporary start and end range
        temp_start = start
        temp_end = end
    else:
        temp_start = start
        temp_end = start + BLOG_END_LIMIT
    
    blog_data = get_blog_data (blog_url, temp_start, temp_end)
    for i in range (0, n_times):#iterate for the more than 50 post range
        for photos in blog_data['posts']:
            photo_urls.append (photos['photo-url-1280'])

        temp_start = temp_end
        if temp_end + BLOG_END_LIMIT < end:# set end range of blog
            temp_end += BLOG_END_LIMIT
        else:
            temp_end = end
        blog_data = get_blog_data (blog_url, temp_start, temp_end)

    return photo_urls


BLOG_URL_PREFIX = "https://"
BLOG_URL_SUFFIX = ".tumblr.com/api/read/json"
ERROR_INVALID = "Something went Wrong: Error Code: 200"
BLOG_END_LIMIT = 50

main ()